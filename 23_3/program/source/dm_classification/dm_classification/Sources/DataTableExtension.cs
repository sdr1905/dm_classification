﻿using dm_classification.Sources.DataTableValueComparer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification.Sources
{
    public static class DataTableExtension
    {
        public static void GetMinMax<T>(this DataTable table, string columnName, Type columnType, out T min, out T max)
        {
            min = default(T);
            max = default(T);
            if (table.Rows.Count != 0)
            {
                var type = typeof(T);
                IValueComparer comparer = ComparerFactory.CreateComparer(columnType);
                object minObj = table.Rows[0][columnName];
                object maxObj = table.Rows[0][columnName];
                foreach (DataRow row in table.Rows)
                {
                    object MiObj = row[columnName];
                    object MaObj = row[columnName];
                    if (IsNotNull(minObj) && IsNotNull(MiObj))
                    {
                        if (comparer.Compare(minObj, MiObj) > 0)
                        {
                            minObj = MiObj;
                        }
                    }
                    else
                    {
                        minObj = GetNotNullObject(minObj, MiObj);
                    }
                    if (IsNotNull(maxObj) && IsNotNull(MaObj))
                    {
                        if (comparer.Compare(maxObj, MaObj) < 0)
                        {
                            maxObj = MaObj;
                        }
                    }
                    else
                    {
                        maxObj = GetNotNullObject(maxObj, MaObj);
                    }
                }
                min = (T)minObj;
                max = (T)maxObj;
            }
        }

        public static double CalculateVariance(this DataTable table, string columnName)
        {
            // S^2 =  (Xi - U)^2 / N-1
            int noOfElement;
            double median = CalculateMedian(table, columnName, out noOfElement);
            double variance = 0;
            foreach (DataRow row in table.Rows)
            {
                if (IsNotNull(row[columnName]))
                {
                    double value = (double)row[columnName];
                    variance += (value - median) * (value - median);
                }
            }
            variance = variance / (noOfElement - 1);
            return variance;
        }

        public static double CalculateMedian(this DataTable table, string columnName, out int numberOfElement)
        {
            double sum = 0;
            try
            {
                numberOfElement = 0;
                foreach (DataRow row in table.Rows)
                {
                    if (IsNotNull(row[columnName]))
                    {
                        numberOfElement++;
                        sum += (double)row[columnName];
                    }
                }
                sum = sum / numberOfElement;
            }
            catch (Exception)
            {
                throw new Exception($"{columnName} is not in numeric type");
            }
            return sum;
        }

        public static double CalculateMedian(this DataTable table, string columnName)
        {
            int n;
            return CalculateMedian(table, columnName, out n);
        }

        public static void NormalizeMinMax(this DataTable table, string columnName, double newMin, double newMax)
        {
            // V' = ((v - min) - (max - min)) * (new max - new min) + new min
            double variance = CalculateVariance(table, columnName);
            double SD = Math.Sqrt(variance);
            double min;
            double max;
            GetMinMax(table, columnName, typeof(double), out min, out max);
            foreach (DataRow row in table.Rows)
            {
                if (IsNotNull(row[columnName]))
                {
                    double value = (double)row[columnName];
                    double newValue = ((value - min) / (max - min)) * (newMax - newMin) + newMin;
                    row[columnName] = newValue;
                }
            }
        }

        public static void NormalizeZScore(this DataTable table, string columnName)
        {
            double variance = CalculateVariance(table, columnName);
            double median = CalculateMedian(table, columnName);
            double SD = Math.Sqrt(variance);
            foreach (DataRow row in table.Rows)
            {
                if (IsNotNull(row[columnName]))
                {
                    double value = (double)row[columnName];
                    double newValue = (value - median) / SD;
                    row[columnName] = newValue;
                }
            }
        }

        public static bool IsNotNull(object obj)
        {
            return obj != DBNull.Value && obj != null;
        }

        private static object GetNotNullObject(object obj1, object obj2)
        {
            if (IsNotNull(obj1))
            {
                return obj1;
            }
            else
            {
                return obj2;
            }
        }

    }
}
