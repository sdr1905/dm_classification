﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification.Sources.Algorithm
{
    public class ID3
    {
        public double CalculateEntropy(DataTable data, string classificationAttribute, int totalTransaction)
        {
            double res = 0;
            Dictionary<string, int> classFrequency = CalculateFrequency(data, classificationAttribute);
            // calculate IG
            foreach (KeyValuePair<string, int> kv in classFrequency)
            {
                double P = ((double)kv.Value / totalTransaction);
                res += -P * Math.Log(P, 2);
            }
            return res;
        }
        private Dictionary<string, int> CalculateFrequency(DataTable data, string attribute)
        {
            Dictionary<string, int> frequency = new Dictionary<string, int>();
            // calculate class label frequency
            foreach (DataRow row in data.Rows)
            {
                string label = row[attribute].ToString();
                if (!frequency.ContainsKey(label))
                {
                    frequency.Add(label, 1);
                }
                else
                {
                    frequency[label]++;
                }
            }
            return frequency;
        }
        private Dictionary<string, double> informationGainsLog = new Dictionary<string, double>();
        public Dictionary<string, DataTable> GetBestSplittingTransactions(DataTable fullData, string classificationAttribute, IEnumerable<string> ignoredAttributes, out string bestSplittingAttr)
        {
            informationGainsLog = new Dictionary<string, double>();
            bestSplittingAttr = "";
            double gainData = this.CalculateEntropy(fullData, classificationAttribute, fullData.Rows.Count);
            Dictionary<string, DataTable> bestAttributeSubTrans = new Dictionary<string, DataTable>();

            double bestGain = double.MinValue;
            foreach (DataColumn dc in fullData.Columns)
            {
                if (dc.ColumnName != classificationAttribute && !ignoredAttributes.Contains(dc.ColumnName))
                {
                    double attIG = 0;
                    var splitedTransactions = Helper.SplitTransactions(fullData, dc.ColumnName);
                    // calculate IG for each transaction
                    foreach (KeyValuePair<string, DataTable> kv in splitedTransactions)
                    {
                        attIG += ((double)kv.Value.Rows.Count / fullData.Rows.Count) * this.CalculateEntropy(kv.Value, classificationAttribute, kv.Value.Rows.Count);
                    }
                    attIG = gainData - attIG;
                    informationGainsLog.Add(dc.ColumnName, attIG);
                    if (bestGain < attIG)
                    {
                        bestAttributeSubTrans = splitedTransactions;
                        bestGain = attIG;
                        bestSplittingAttr = dc.ColumnName;
                    }
                }
            }
            return bestAttributeSubTrans;
        }
        private int RecursionDepth = 0;
        public List<ID3Log> Logs = new List<ID3Log>();
        private DecisionTree _DTree = null;
        private string MostCommonClass = null;
        public void BuildDecisionTree(DataTable data, string classificationAttribute, IEnumerable<string> ignoredAttributes, DNode parent)
        {
            var gainData = this.CalculateEntropy(data, classificationAttribute, data.Rows.Count);
            if (gainData != 0 && ignoredAttributes.Count() == data.Columns.Count - 1)
            {
                DNode classNode = new DNode() { AttributeName = classificationAttribute, Value = MostCommonClass };
                parent.Childs.Add(classNode);
                // log
                ID3Log log = new ID3Log();
                log.Level = RecursionDepth;
                log.Decision = classNode.Value;
                Logs.Add(log);
                return;
            }
            if (gainData == 0)
            {
                DNode classNode = new DNode() { AttributeName = classificationAttribute, Value = data.Rows[0][classificationAttribute] };
                parent.Childs.Add(classNode);
                // log
                ID3Log log = new ID3Log();
                log.Level = RecursionDepth;
                log.Decision = classNode.Value;
                Logs.Add(log);
                // 
            }
            else
            {
                string bsa;
                var transactions = GetBestSplittingTransactions(data, classificationAttribute, ignoredAttributes, out bsa);
                // log
                ID3Log log = new ID3Log();
                log.Level = RecursionDepth;
                log.SelectedAttribute = bsa;
                log.InformationGains = informationGainsLog;
                Logs.Add(log);
                // recursively call for sub transactions
                foreach (KeyValuePair<string, DataTable> kv in transactions)
                {
                    DNode classNode = new DNode() { AttributeName = bsa, Value = kv.Key };
                    parent.Childs.Add(classNode);
                    RecursionDepth++;
                    BuildDecisionTree(kv.Value, classificationAttribute, ignoredAttributes.Concat(new string[] { bsa }), classNode);
                }
            }
        }
        public DecisionTree BuildDecisionTree(DataTable data, string classificationAttribute)
        {
            List<string> ignoredAttrs = new List<string>();
            Dictionary<string, int> classFrequency = CalculateFrequency(data, classificationAttribute);
            MostCommonClass = classFrequency.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
            _DTree = new DecisionTree();
            _DTree.ClassificationAttribute = classificationAttribute;
            this.BuildDecisionTree(data, classificationAttribute, ignoredAttrs, _DTree.Root);
            if (_DTree.Root.Childs.Count != 0)
            {
                _DTree.Root.AttributeName = _DTree.Root.Childs.First().AttributeName;
            }
            return _DTree;
        }
        public void Predict(DataTable transactions, DecisionTree dtree)
        {
            transactions.Columns.Add(new DataColumn() { ColumnName = dtree.ClassificationAttribute, DataType = dtree.ClassificationAttribute.GetType() });
            foreach (DataRow row in transactions.Rows)
            {
                DNode node = dtree.Root;
                while (node != null && node.Childs.Count > 0)
                {
                    if (node.Childs.Count == 1 && node.Childs[0].AttributeName == dtree.ClassificationAttribute)
                    {
                        node = node.Childs[0];
                    }
                    foreach (DNode n in node.Childs)
                    {
                        if (row[n.AttributeName].ToString().Equals(n.Value.ToString()))
                        {
                            node = n;
                            break;
                        }
                    }
                }
                row[dtree.ClassificationAttribute] = node.Value;
            }
        }
        public void ExportModel(DecisionTree dtree, string fileName)
        {
            StreamWriter sw = new StreamWriter(fileName + ".id3");
            sw.WriteLine(dtree.ClassificationAttribute);
            Queue<DNode> nodes = new Queue<DNode>();
            nodes.Enqueue(dtree.Root);
            while (nodes.Count > 0)
            {
                DNode n = nodes.Dequeue();
                sw.WriteLine(n.AttributeName + "," + n.Value + "," + n.Childs.Count);
                foreach (DNode item in n.Childs)
                {
                    nodes.Enqueue(item);
                }
            }
            sw.Flush();
            sw.Close();
        }
        public DecisionTree ImportModel(string id3ModelFile)
        {
            StreamReader sr = new StreamReader(id3ModelFile);
            DecisionTree dtree = new DecisionTree();
            Queue<Dictionary<DNode, int>> queue = new Queue<Dictionary<DNode, int>>();
            string line = sr.ReadLine();
            dtree.ClassificationAttribute = line;
            line = sr.ReadLine();
            string[] tks = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            string attName = tks[0];
            int child = int.Parse(tks[tks.Length - 1]);
            Dictionary<DNode, int> dicNode = new Dictionary<DNode, int>();
            dicNode.Add(dtree.Root, child);
            queue.Enqueue(dicNode);
            while (queue.Count > 0)
            {
                dicNode = queue.Dequeue();
                child = dicNode.Values.First();
                if (!sr.EndOfStream)
                {
                    for (int i = 0; i < child; i++)
                    {
                        line = sr.ReadLine();
                        tks = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        attName = tks[0];
                        string value = tks[1];
                        int c = int.Parse(tks[tks.Length - 1]);
                        DNode n = new DNode()
                        {
                            AttributeName = attName,
                            Value = value
                        };
                        dicNode.Keys.First().Childs.Add(n);
                        var tmp = new Dictionary<DNode, int>();
                        tmp.Add(n, c);
                        queue.Enqueue(tmp);
                    }
                }
            }
            return dtree;
        }
        public void SaveLog(string logFile)
        {
            StreamWriter sw = new StreamWriter(logFile);
            if (this.Logs.Count != 0)
            {
                foreach (ID3Log log in Logs)
                {
                    sw.WriteLine("[LEVEL] " + log.Level);
                    if (log.Decision != null)
                    {
                        sw.WriteLine("\tDecision = " + log.Decision);
                    }
                    foreach (KeyValuePair<string,double> ig in log.InformationGains)
                    {
                        sw.WriteLine("\tInformation gain: " + ig.Key + "  " + ig.Value);
                    }
                    sw.WriteLine("\tAttribute selected: " + (string.IsNullOrEmpty(log.SelectedAttribute) ? "NONE" : log.SelectedAttribute));
                }
            }
            sw.Flush();
            sw.Close();
        }
    }
}
