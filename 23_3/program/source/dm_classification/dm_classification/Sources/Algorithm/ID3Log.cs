﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification.Sources.Algorithm
{
    public class ID3Log
    {
        public int Level { get; set; }
        public Dictionary<string, double> InformationGains { get; set; }
        public object Decision { get; set; }
        public string SelectedAttribute { get; set; }
        public ID3Log()
        {
            InformationGains = new Dictionary<string, double>();
        }
    }
}
