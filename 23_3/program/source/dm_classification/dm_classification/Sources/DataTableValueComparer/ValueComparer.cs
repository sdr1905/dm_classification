﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification.Sources.DataTableValueComparer
{
    public interface IValueComparer
    {
        int Compare(object obj1, object obj2);
    }
    public class DoubleComparer : IValueComparer
    {
        public int Compare(object obj1, object obj2)
        {
            double d1 = double.Parse(obj1.ToString());
            double d2 = double.Parse(obj2.ToString());
            return d1 > d2 ? 1 : d1 == d2 ? 0 : -1;
        }
    }
    public class DecimalComparer : IValueComparer
    {
        public int Compare(object obj1, object obj2)
        {
            decimal d1 = (decimal)obj1;
            decimal d2 = (decimal)obj2;
            return d1 > d2 ? 1 : d1 == d2 ? 0 : -1;
        }
    }
    public class IntComparer : IValueComparer
    {
        public int Compare(object obj1, object obj2)
        {
            return (int)obj1 - (int)obj2;
        }
    }

    public class CustomStringComparer : IValueComparer
    {
        public int Compare(object obj1, object obj2)
        {
            return obj1.ToString().CompareTo(obj2.ToString());
        }
    }
    public class DateTimeComparer : IValueComparer
    {
        public int Compare(object obj1, object obj2)
        {
            return ((DateTime)obj1).CompareTo((DateTime)obj2);
        }
    }

    public class ComparerFactory
    {
        public static IValueComparer CreateComparer(Type type)
        {
            switch (type.FullName)
            {
                case "System.String": return new CustomStringComparer();
                case "System.Boolean": return new CustomStringComparer();
                case "System.DateTime": return new DateTimeComparer();
                case "System.Decimal": return new DecimalComparer();
                case "System.Double": return new DoubleComparer();
                case "System.Int32": return new IntComparer();
            }
            return null;
        }
    }

}
