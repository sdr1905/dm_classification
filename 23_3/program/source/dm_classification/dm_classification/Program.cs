﻿using dm_classification.Sources;
using dm_classification.Sources.Algorithm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0 || args.Length % 2 != 0)
            {
                PrintMenu();
                return;
            }
            // parse parameters
            try
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                for (int i = 0; i < args.Length-1; i += 2)
                {
                    parameters.Add(args[i], args[i + 1]);
                }
                string task = parameters.First(x => x.Key == "-task").Value;
                string infile = parameters.First(x => x.Key == "-input").Value;
                string outFile = parameters.First(x => x.Key == "-output").Value;
                ExcelDataPresenter presenter = new ExcelDataPresenter(infile);
                Out("Reading data...");
                DataTable data = presenter.GetSheetData(0);
                Out("Read " + data.Rows.Count + " rows");
                ID3 id3 = new ID3();
                switch (task)
                {
                    case "train":
                        if (!parameters.ContainsKey("-classAtt") || !parameters.ContainsKey("-log"))
                        {
                            throw new ArgumentNullException("options", "Arguments missed");
                        }
                        string classAttr = parameters.First(x => x.Key == "-classAtt").Value;
                        string logFile = parameters.First(x => x.Key == "-log").Value;
                        var tree = id3.BuildDecisionTree(data, classAttr);
                        id3.ExportModel(tree, outFile);
                        id3.SaveLog(logFile);
                        Out("Exported model to " + outFile);
                        Out("Saved log to " + logFile);
                        break;
                    case "predict":
                        string modelFile = parameters.First(x => x.Key == "-model").Value;
                        Out("Reading decision tree...");
                        var modelTree = id3.ImportModel(modelFile);
                        Out("Applying model to predict...");
                        id3.Predict(data, modelTree);
                        var p = new ExcelDataPresenter(outFile);
                        Out("Saving to excel...");
                        p.SaveToExcel(data, "OUT_PREDICTED", true);
                        Out(data.Rows.Count + " rows was saved to " + outFile);
                        break;
                }
                Console.Out.Flush();
            }
            catch (Exception ex)
            {
                if (ex is ArgumentNullException)
                {
                    Out(ex.Message);
                    PrintMenu();
                }
                else
                {
                    Out("An error occured: \n\t" + ex.Message);
                    Out("Program is terminated.");
                }
                return;
            }
            Environment.Exit(0);
        }
        static void PrintMenu()
        {
            Out("Syntax: ");
            Out("23_3 -task [task name] -input [input.xslx] -output [output file] [advance options]");
            Out("\t[task name] can be : train | predict");
            Out("\tinput file must be an xlsx file.");
            Out("\t[output file] is an id3 file if task = train, or an xlsx file if task = predict");
            Out("\t[advance options] can be:");
            Out("\ttask = train");
            Out("\t\t-classAtt [classification attribute] : specify which attribute is used to classify");
            Out("\t\t-log [log file] : specify logging file during classification");
            Out("");
            Out("\ttask = predict");
            Out("\t\t-model [model.id3] : a model file use to predict, must be an id3 file");
            Out("");
            Out("Example:\\23_3 -task train -input in.xlsx -output out.xlsx -log log.txt");
        }
        static void Out(string msg)
        {
            Console.WriteLine(msg);
        }
        //static void Main(string[] args)
        //{
        //    ExcelDataPresenter presenter = new ExcelDataPresenter("asdf.xlsx");
        //    DataTable data = presenter.GetSheetData(0);
        //    ID3 id3 = new ID3();
        //    string classAttr = "play";
        //    var tree = id3.BuildDecisionTree(data, classAttr);
        //    var logs = id3.Logs;
        //    presenter = new ExcelDataPresenter("test.xlsx");
        //    var testData = presenter.GetSheetData(0);
        //    id3.Predict(testData, tree);
        //    id3.ExportModel(tree, "model");
        //    DecisionTree dtree =  id3.ImportModel("model.id3");
        //}
    }
}
