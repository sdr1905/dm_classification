﻿using dm_classification.Sources.DataTableValueComparer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace dm_classification.Sources
{
    public class ExcelDataPresenter
    {
        private string ConnectionString { get; set; }

        public string FileName { get; set; }

        public ExcelDataPresenter(string fileName)
        {
            bool isRootPath = System.IO.Path.IsPathRooted(fileName);
            try
            {
                //if (!System.IO.File.Exists(fileName))
                //{
                //    throw new FileNotFoundException("File name does not exist", fileName);
                //}
                string path = string.Empty;
                if (isRootPath)
                {
                    path = fileName;
                }
                else
                {
                    // try combine path with current assembly location
                    var currentExecutingLocation = System.IO.Directory.GetCurrentDirectory();
                    path = System.IO.Path.Combine(currentExecutingLocation, fileName);
                }
                string connStringFormat = ConfigurationManager.AppSettings["ExcelConnStringFormat"];
                FileName = path;
                this.ConnectionString = string.Format(connStringFormat, path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetData(string excelQuery)
        {
            OleDbConnection db = new OleDbConnection(ConnectionString);
            db.Open();
            var adapter = new OleDbDataAdapter(excelQuery, db);
            var tbl = new DataTable();
            OleDbCommand cmd = db.CreateCommand();
            cmd.CommandText = excelQuery;
            var reader = cmd.ExecuteReader();
            adapter.Fill(tbl);
            db.Close();
            return tbl;
        }

        public DataTable GetSheetData(int index)
        {
            string[] sheets = GetSheetName();
            string query = $"select * from [{sheets[index].Trim('$')}$]";
            return GetData(query);
        }

        public int SaveToExcel(DataTable table, string outSheet, bool deleteExisted)
        {
            if (deleteExisted)
            {
                System.IO.File.Delete(FileName);
            }
            OleDbConnection db = new OleDbConnection(ConnectionString);
            db.Open();
            var command = db.CreateCommand();
            command.CommandText = GetCreateTableString(table, outSheet);
            command.ExecuteNonQuery();
            int affectedRows = 0;
            string colNames = GetColumnNames(table);
            int i = 0;
            StringBuilder sb = new StringBuilder($"insert into [{outSheet}$] ({colNames}) values ({GetPreparingValueWildcards(table.Columns.Count)})");
            OleDbCommand cmd = new OleDbCommand(sb.ToString(), db);
            AddParameters(cmd, table.Columns);
            cmd.Prepare();
            foreach (DataRow dr in table.Rows)
            {
                foreach (DataColumn dc in table.Columns)
                {
                    cmd.Parameters[dc.ColumnName].Value = dr.IsNull(dc) ? DBNull.Value : dr[dc];
                }
                affectedRows += cmd.ExecuteNonQuery();
            }
            db.Close();
            return affectedRows;
        }

        #region Preprocessing

        public int RemoveMissingInstance(DataTable table, params string[] columns)
        {
            int rowsRemoved = 0;
            if (columns != null)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    foreach (string c in columns)
                    {
                        if (table.Columns.Contains(c))
                        {
                            if (!DataTableExtension.IsNotNull(table.Rows[i][c]))
                            {
                                table.Rows.Remove(table.Rows[i]);
                                i--;
                                rowsRemoved++;
                                break;
                            }
                        }
                    }
                }
            }
            return rowsRemoved;
        }

        public void NormalizeMinMax(DataTable table, string columnName, double newMin, double newMax)
        {
            // V' = ((v - min) - (max - min)) * (new max - new min) + new min
            double variance = CalculateVariance(table, columnName);
            double SD = Math.Sqrt(variance);
            double min;
            double max;
            GetMinMax(table, columnName, out min, out max);
            foreach (DataRow row in table.Rows)
            {
                if (IsNotNull(row[columnName]))
                {
                    double value = double.Parse(row[columnName].ToString());
                    double newValue = ((value - min) / (max - min)) * (newMax - newMin) + newMin;
                    row[columnName] = newValue;
                }
            }
        }

        public void NormalizeMinMax(DataTable table, double newMin, double newMax, string[] columnNames)
        {
            if (columnNames != null)
            {
                foreach (string col in columnNames)
                {
                    NormalizeMinMax(table, col, newMin, newMax);
                }
            }
        }

        public void NormalizeZScore(DataTable table, string[] columnNames)
        {
            if (columnNames != null)
            {
                foreach (string col in columnNames)
                {
                    NormalizeZScore(table, col);
                }
            }
        }

        public void NormalizeZScore(DataTable table, string columnName)
        {
            double variance = CalculateVariance(table, columnName);
            double median = CalculateMedian(table, columnName);
            double SD = Math.Sqrt(variance);
            foreach (DataRow row in table.Rows)
            {
                if (IsNotNull(row[columnName]))
                {
                    double value = double.Parse(row[columnName].ToString());
                    double newValue = (value - median) / SD;
                    row[columnName] = newValue;
                }
            }
        }

        public int RemoveColumns(DataTable table, params string[] columns)
        {
            int columnsRemoved = 0;
            if (columns != null)
            {
                foreach (string c in columns)
                {
                    if (table.Columns.Contains(c))
                    {
                        table.Columns.Remove(c);
                        columnsRemoved++;
                    }
                }
            }
            return columnsRemoved;
        }

        public int RemoveRowsMissingValue(DataTable table, params string[] columns)
        {
            int rowsRemoved = 0;
            if (columns != null)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    bool needRemove = false;
                    foreach (string c in columns)
                    {
                        if (!IsNotNull(table.Rows[i][c]))
                        {
                            needRemove = true;
                            break;
                        }
                    }
                    if (needRemove)
                    {
                        table.Rows.Remove(table.Rows[i]);
                        i--;
                        rowsRemoved++;
                    }
                }
            }
            return rowsRemoved;
        }

        public void BinningWidth(DataTable table, int bin, string column)
        {
            double min;
            double max;
            string binnedColumnInfo = "Bin_Width_" + column;
            var comparer = ComparerFactory.CreateComparer(typeof(double));
            table.Columns.Add(binnedColumnInfo, typeof(string));
            GetMinMax(table, column, out min, out max);
            double binWidth = (max - min) / bin;
            double binValue = min + binWidth;
            int binBlock = 1;
            int i = 0;
            for (; i < table.Rows.Count; i++)
            {
                object value = table.Rows[i][column];
                if (IsNotNull(value))
                {
                    if (comparer.Compare(value, binValue) > 0)
                    {
                        binValue += binWidth;
                        NewBin(table, binnedColumnInfo, i++, binBlock++);
                    }
                }
            }
            NewBin(table, binnedColumnInfo, i, binBlock);
        }

        private void NewBin(DataTable table, string infoColumn, int row, int binBlock)
        {
            DataRow emptyRow = table.NewRow();
            emptyRow[infoColumn] = "Bin " + binBlock++;
            table.Rows.InsertAt(emptyRow, row);
        }
        public void BinningWidth(DataTable table, int bin, params string[] columns)
        {
            if (columns != null)
            {
                foreach (string col in columns)
                {
                    try
                    {
                        SortAsc(table, col, typeof(double));
                        BinningWidth(table, bin, col);
                    }
                    catch (Exception ex)
                    {
                        if (ex is InvalidCastException)
                        {
                            throw new Exception($"{col} is not number.");
                        }
                    }
                }
            }
        }

        public void BinningFrequency(DataTable table, int bin, params string[] columns)
        {
            if (columns != null)
            {
                foreach (string col in columns)
                {
                    try
                    {
                        BinningFrequency(table, bin, col);
                    }
                    catch (Exception ex)
                    {
                        if (ex is InvalidCastException)
                        {
                            throw new Exception($"{col} is not number.");
                        }
                    }
                }
            }
        }

        public void BinningFrequency(DataTable table, int bin, string column)
        {
            double min;
            double max;
            string binnedColumnInfo = "Bin_Frequency_" + column;
            var comparer = ComparerFactory.CreateComparer(typeof(double));
            table.Columns.Add(binnedColumnInfo, typeof(string));
            GetMinMax(table, column, out min, out max);

            double elementOfEachBin = (double)table.Rows.Count / bin;
            int binBlock = 1;
            int approximate = (int)Math.Ceiling(elementOfEachBin);
            //double floatPoint = -(approximate - elementOfEachBin);
            //if (floatPoint > 0.5)
            //{
            //    approximate = Math.Floor(elementOfEachBin);
            //    floatPoint = elementOfEachBin - approximate;
            //}
            int countDown = approximate;
            int i = 0;
            for (i = 0; i < table.Rows.Count; i++)
            {
                object value = table.Rows[i][column];
                if (IsNotNull(value))
                {
                    countDown--;
                    if (countDown < 0)
                    {
                        countDown = approximate;
                        NewBin(table, binnedColumnInfo, i++, binBlock++);
                    }
                }
            }
            NewBin(table, binnedColumnInfo, table.Rows.Count, binBlock);
        } 
        #endregion

        #region utils
        private bool IsNumber(Type type)
        {
            return type == typeof(double) || type == typeof(int) || type == typeof(decimal);
        }

        private void SwapObject(object obj1, object obj2)
        {
            object tmp = obj1;
            obj1 = obj2;
            obj2 = tmp;
        }

        private void SwapRow(DataRow r1, int i, DataRow r2, int j)
        {
            var newRowr1 = r1.Table.NewRow();
            newRowr1.ItemArray = r2.ItemArray;
            var newRowr2 = r2.Table.NewRow();
            newRowr2.ItemArray = r1.ItemArray;

            r1.Table.Rows.RemoveAt(i);
            r1.Table.Rows.InsertAt(newRowr1, i);

            r1.Table.Rows.RemoveAt(j);
            r1.Table.Rows.InsertAt(newRowr2, j);
        }

        public void SortAsc(DataTable table, string columnName, Type columnType)
        {
            if (table.Rows.Count > 0)
            {
                var comparer = ComparerFactory.CreateComparer(columnType);
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    for (int j = table.Rows.Count - 1; j > i; j--)
                    {
                        object obj1 = table.Rows[i][columnName];
                        object obj2 = table.Rows[j][columnName];
                        if (IsNotNull(obj1) && IsNotNull(obj2))
                            if (comparer.Compare(obj1, obj2) > 0)
                            {
                                SwapRow(table.Rows[i], i, table.Rows[j], j);
                            }
                    }
                }
            }
        }

        private void AddParameters(OleDbCommand command, DataColumnCollection columns)
        {
            foreach (DataColumn dc in columns)
            {
                command.Parameters.Add(dc.ColumnName, MapOleDbType(dc.DataType), MapOleDbSize(dc.DataType));
            }
        }

        private string GetPreparingValueWildcards(int colNum)
        {
            string res = "";
            for (int i = 0; i < colNum; i++)
            {
                res += "?,";
            }
            return res.Trim(',');
        }

        private OleDbCommand PrepareInsertCommand(string sheetName, string colNames, int colNumber, OleDbConnection connection)
        {
            string preparedValues = string.Empty;
            for (int i = 0; i < colNumber; i++)
            {
                preparedValues += "?,";
            }
            string text = $"insert into [{sheetName}$] ({colNames}) values ({preparedValues.Trim(',')})";
            var command = connection.CreateCommand();
            command.CommandText = text;
            return command;
        }

        private string[] GetSheetName()
        {
            OleDbConnection db = new OleDbConnection(ConnectionString);
            db.Open();
            var tbl = db.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string[] res = new string[tbl.Rows.Count];
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                res[i] = tbl.Rows[i]["TABLE_NAME"].ToString();
                return res;
            }
            db.Close();
            return res;
        }

        private string GetTableValueString(DataRow row)
        {
            string res = string.Empty;
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                res += $"'{row[i].ToString().Replace("'", "''")}' ,";
            }
            return $"({res.Trim(',').Trim(' ')});";
        }

        private string GetColumnNames(DataTable table)
        {
            string res = string.Empty;
            foreach (DataColumn col in table.Columns)
            {
                res += col.ColumnName + ",";
            }
            return $"{res.Trim(',')}";
        }

        private string GetCreateTableString(DataTable table, string tableName)
        {
            string res = string.Empty;
            foreach (DataColumn col in table.Columns)
            {
                res += $"{col.ColumnName} {MapExcelDbType(col.DataType)}, ";
            }
            res = $"create table [{tableName}] ({res.Trim(' ').Trim(',')});";
            return res;
        }

        private string MapExcelDbType(Type systemType)
        {
            switch (systemType.FullName)
            {
                case "System.String": return "Text";
                case "System.Boolean": return "Text";
                case "System.DateTime": return "Date";
                case "System.Decimal": return "Numeric";
                case "System.Double": return "Numeric";
                case "System.Int32": return "Numeric";
            }
            return null;
        }

        private OleDbType MapOleDbType(Type systemType)
        {
            switch (systemType.FullName)
            {
                case "System.String": return OleDbType.VarWChar;
                case "System.Boolean": return OleDbType.VarChar;
                case "System.DateTime": return OleDbType.Date;
                case "System.Decimal": return OleDbType.Decimal;
                case "System.Double": return OleDbType.Double;
                case "System.Int32": return OleDbType.Integer;
            }
            return OleDbType.IUnknown;
        }

        private int MapOleDbSize(Type systemType)
        {
            switch (systemType.FullName)
            {
                case "System.String": return 255;
                case "System.Boolean": return 5;
                case "System.Decimal": return sizeof(decimal);
                case "System.Double": return sizeof(double);
                case "System.Int32": return sizeof(int);
            }
            return -1;
        }

        private bool IsString(Type systemType)
        {
            return systemType.FullName == "System.String";
        }

        private void GetMinMax(DataTable table, string columnName, out double min, out double max)
        {
            min = 0;
            max = 0;
            if (table.Rows.Count != 0)
            {
                IValueComparer comparer = ComparerFactory.CreateComparer(typeof(double));
                object minObj = table.Rows[0][columnName];
                object maxObj = table.Rows[0][columnName];
                foreach (DataRow row in table.Rows)
                {
                    object MiObj = row[columnName];
                    object MaObj = row[columnName];

                    if (IsNotNull(minObj) && IsNotNull(MiObj))
                    {
                        if (comparer.Compare(minObj, MiObj) > 0)
                        {
                            minObj = MiObj;
                        }
                    }
                    else
                    {
                        minObj = GetNotNullObject(minObj, MiObj);
                    }

                    if (IsNotNull(maxObj) && IsNotNull(MaObj))
                    {
                        if (comparer.Compare(maxObj, MaObj) < 0)
                        {
                            maxObj = MaObj;
                        }
                    }
                    else
                    {
                        maxObj = GetNotNullObject(maxObj, MaObj);
                    }
                }
                min = double.Parse(minObj.ToString());
                max = double.Parse(maxObj.ToString());
            }
        }

        private double CalculateVariance(DataTable table, string columnName)
        {
            // S^2 =  (Xi - U)^2 / N-1
            int noOfElement;
            double median = CalculateMedian(table, columnName, out noOfElement);
            double variance = 0;
            foreach (DataRow row in table.Rows)
            {
                if (IsNotNull(row[columnName]))
                {
                    double value = double.Parse(row[columnName].ToString());
                    variance += (value - median) * (value - median);
                }
            }
            variance = variance / (noOfElement - 1);
            return variance;
        }

        private double CalculateMedian(DataTable table, string columnName, out int numberOfElement)
        {
            double sum = 0;
            try
            {
                numberOfElement = 0;
                foreach (DataRow row in table.Rows)
                {
                    if (IsNotNull(row[columnName]))
                    {
                        numberOfElement++;
                        sum += double.Parse(row[columnName].ToString());
                    }
                }
                sum = sum / numberOfElement;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sum;
        }

        private double CalculateMedian(DataTable table, string columnName)
        {
            int n;
            return CalculateMedian(table, columnName, out n);
        }

        private bool IsNotNull(object obj)
        {
            return obj != DBNull.Value && obj != null;
        }

        private object GetNotNullObject(object obj1, object obj2)
        {
            if (IsNotNull(obj1))
            {
                return obj1;
            }
            else
            {
                return obj2;
            }
        }
        #endregion
    }
}
