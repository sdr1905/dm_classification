﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification.Sources.Algorithm
{
    public class DecisionTree
    {
        public string ClassificationAttribute { get; set; }
        public DNode Root { get; set; }
        public DecisionTree()
        {
            Root = new DNode();
        }
    }
}
