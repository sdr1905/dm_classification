﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dm_classification.Sources.Algorithm
{
    public static class Helper
    {
        public static Dictionary<string, DataTable> SplitTransactions(DataTable originalTransaction, string splittingAttribute)
        {
            Dictionary<string, DataTable> splitedTransactions = new Dictionary<string, DataTable>();
            foreach (DataRow row in originalTransaction.Rows)
            {
                string rowData = row[splittingAttribute].ToString();
                if (splitedTransactions.ContainsKey(rowData))
                {
                    splitedTransactions[rowData].ImportRow(row);
                }
                else
                {
                    DataTable newTable = originalTransaction.Clone();
                    newTable.ImportRow(row);
                    splitedTransactions.Add(rowData, newTable);
                }
            }
            return splitedTransactions;
        }
    }
}
